# Generated by Django 4.0.3 on 2022-05-02 12:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='createblog',
            name='created_on',
            field=models.DateField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='createblog',
            name='published',
            field=models.BooleanField(default=False, verbose_name='Publié'),
        ),
        migrations.AlterField(
            model_name='createblog',
            name='body',
            field=models.TextField(blank=True, verbose_name='Contenu'),
        ),
        migrations.AlterField(
            model_name='createblog',
            name='slug',
            field=models.SlugField(blank=True, max_length=255, unique=True),
        ),
        migrations.AlterField(
            model_name='createblog',
            name='title',
            field=models.CharField(max_length=255, unique=True, verbose_name='Titre'),
        ),
    ]
