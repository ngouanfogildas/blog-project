from pyexpat import model
from django.db import models
from matplotlib import image
from django.template.defaultfilters import slugify

class CreateBlog(models.Model):
    title = models.CharField(max_length=255, unique=True, verbose_name="Titre")
    slug = models.SlugField(max_length=255, unique=True, blank=True)
    intro = models.TextField()
    body = models.TextField(blank=True, verbose_name="Contenu")
    image = models.ImageField(upload_to='media')
    #author = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)
    created_on = models.DateField(blank=True, null=True)
    published = models.BooleanField(default=False, verbose_name="Publié")
    date_added = models.DateTimeField(auto_now=True)

    def __str__(self) -> str:
        return self.title

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.title)

        super().save(*args, **kwargs)

    class Meta:
        ordering = ['-date_added']

class Comment(models.Model):
    post = models.ForeignKey(CreateBlog, related_name='comments', on_delete=models.CASCADE)
    name = models.CharField(max_length=50, default="inconue")
    email = models.EmailField()
    body = models.TextField()
    date_added = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['-date_added']

